// https://github.com/espressif/esp-dl/blob/master/examples/face_recognition/main/app_main.cpp
// https://github.com/espressif/esp-dl/blob/idfv5.0/include/model_zoo/face_recognizer.hpp
#include <iostream>
#include <vector>
#include <stdio.h>
#include <sys/stat.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <esp_log.h>

#include "sdkconfig.h"
#include "esp_vfs.h"
#include "storage.h"

#include "human_face_detect_msr01.hpp"
#include "human_face_detect_mnp01.hpp"
#include "dl_tool.hpp"
#include "who_ai_utils.hpp"

#include "image.h"
#include "face_recognition.h"
#include "face_recognition_tool.hpp"


static const char *TAG = "face_recognition";

// Print enrolled ids
void face_recognition_enrolled_print(FR &recognizer)
{
    using namespace std;
    // Get the index of the enrolled ids 
    vector<face_info_t> ids = recognizer.get_enrolled_ids();
    for (vector<face_info_t>::iterator i = ids.begin(); i != ids.end(); ++i) {
        cout << "name: " << (*i).name << " id: " << (*i).id << "\n";
    }
}

// Get enrolled name from id
std::string face_recognition_name(FR &recognizer, int id)
{
    using namespace std;
    std::string name;
    vector<face_info_t> ids = recognizer.get_enrolled_ids();
    for (vector<face_info_t>::iterator i = ids.begin(); i != ids.end(); ++i) {
        //cout << "name: " << (*i).name << " id: " << (*i).id << "\n"; // DEBUG!!!
        if (id == (*i).id) {
            name = (*i).name;
            break;
        }
    }
    return name;
}


// Enroll face from memory jpg
int face_recognition_enroll(FR &recognizer, uint8_t *image_jpg, size_t image_size, const char *name)
{
    using namespace std;  
    
    size_t width = 0;
    size_t height = 0;
    uint8_t *out = NULL;

    bool status = image_decode_jpg_scale(image_jpg, image_size, 
                            &width, &height, &out, JPEG_IMAGE_FORMAT_RGB888, JPEG_IMAGE_SCALE_0);
    if(status == false && out) {
        free(out);
        return false;
    }
    
    uint8_t *image_bgr = out;
    int image_height = height; 
    int image_width = width;
    int image_channel = 3;
    
    HumanFaceDetectMSR01 s1(0.1F, 0.5F, 10, 0.2F);
    HumanFaceDetectMNP01 s2(0.5F, 0.3F, 5);
    
    //ESP_LOGI(TAG, "height: %d width: %d", height, width); // DEBUG!!!
    std::list<dl::detect::result_t> &candidates = s1.infer((uint8_t *)image_bgr, {image_height, image_width, image_channel});
    std::list<dl::detect::result_t> &results = s2.infer((uint8_t *)image_bgr, {image_height, image_width, image_channel}, candidates);
    
    if (results.size() < 1) {
        ESP_LOGE(TAG, "(%s:%d) no landmarks", __func__, __LINE__);
        free(out);
        return -1;
    }
    
    vector<int> landmarks = results.front().keypoint;
    
    Tensor<uint8_t> image_rgb888;
    image_rgb888.set_element((uint8_t *)image_bgr).set_shape({image_height, image_width, image_channel}).set_auto_free(false);
        
    bool update_flash = false; // the enrolled ids will not be stored to flash
    int id = recognizer.enroll_id(image_rgb888, landmarks, name, update_flash);
    
    ESP_LOGI(TAG, "[enroll] id: %d name: %s", id, name);
            
    free(out);
    
    return id;
}

// Enroll face from jpg file
int face_recognition_enroll(FR &recognizer, const char *path, const char *name)
{
    struct stat st = {};
    if (stat(path, &st) != 0)   { // file not found
        perror(path);
        return false;
    }
    
    size_t image_size = st.st_size; // file size
    if (image_size == 0)
        return false;
            
    uint8_t *image_jpg = (uint8_t *)heap_caps_malloc(image_size, MALLOC_CAP_8BIT | MALLOC_CAP_SPIRAM);
    if (image_jpg == NULL)
        return false;
    
    size_t bytes = 0;
    FILE *fp = fopen(path, "rb");
    if (fp) {
        bytes = fread(image_jpg, 1, image_size, fp);
        fclose(fp);        
        if (bytes != image_size) {
            heap_caps_free(image_jpg);
            return false;
        }
    }
    
    ESP_LOGI(TAG, "image path: %s", path);
    
    int id = face_recognition_enroll(recognizer, image_jpg, image_size, name);
    heap_caps_free(image_jpg);
    
    return id;
}

// Enroll faces from jpg directory
// Sato_23.jpg -> Sato_23 as name
int face_recognition_enroll(FR &recognizer, const char *path_dir)
{
    DIR *dir = opendir(path_dir);
    if (dir == NULL) {
        ESP_LOGI(TAG, "Cannot open directory '%s'", path_dir);
        return -1;
    }

    int res = 0;
    int id = 0;
    int size = 0;    

    struct dirent *entry;    
    
    while ( (entry = readdir(dir)) != NULL) {
        if (entry->d_type != DT_REG) // not regular file
            continue;
            
        char path[100] = "";
        strcat(path, path_dir);
        strcat(path, "/");
        strcat(path, entry->d_name);
        
        size = storage_file_size(path);
        if (size <= 0) {
            res = -1;
            break; // error
        }
        
        char name[100] = "";
        strcpy(name, entry->d_name);
        
        char *sep = strrchr(name, '.'); // Sato_23.jpg -> Sato_23
        if (sep == NULL) {
            res = -1;
            break; // error                
        }
        *sep = '\0';
                        
        id = face_recognition_enroll(recognizer, path, name); 
        if (id < 0) {
            res = -1;
            break;   
        }       
    } // while
    closedir(dir);
    return res;
}

// Recognize face from memory raw
int face_recognition_recognize(FR &recognizer, const uint8_t *image_bgr, int height, int width, int channel)
{
    using namespace std;    
    
    HumanFaceDetectMSR01 s1(0.1F, 0.5F, 10, 0.2F);
    HumanFaceDetectMNP01 s2(0.5F, 0.3F, 5);  
    
    std::list<dl::detect::result_t> &candidates = s1.infer((uint8_t *)image_bgr, {height, width, channel});
    std::list<dl::detect::result_t> &results = s2.infer((uint8_t *)image_bgr, {height, width, channel}, candidates);
    
    if (results.size() < 1) {
        // ESP_LOGE(TAG, "(%s:%d) no landmarks", __func__, __LINE__);
        return -1;
    }

    // display    
#ifdef CONFIG_FACE_DETECTION_PRINT
    print_detection_result(results);
#endif // CONFIG_FACE_DETECTION_PRINT
    
    vector<int> landmarks = results.front().keypoint;
    
    // https://github.com/espressif/esp-dl/blob/master/include/typedef/dl_variable.hpp
    Tensor<uint8_t> image_rgb888;
    image_rgb888.set_element((uint8_t *)image_bgr).set_shape({height, width, channel}).set_auto_free(false); 
        
#ifdef CONFIG_FACE_DETECTION_DRAW
    draw_detection_result((uint8_t *)image_bgr, height, width, results);    
#endif // CONFIG_FACE_DETECTION_DRAW
    
    // recognize face
    face_info_t recognize = recognizer.recognize(image_rgb888, landmarks);
    ESP_LOGI(TAG, "[recognition result] id: %d name: %s similarity: %5.2f", 
                recognize.id, recognize.name.c_str(), recognize.similarity);
    
    return recognize.id;
} // face_recognition

// Recognize face from memory jpg
int face_recognition_recognize(FR &recognizer, uint8_t *image_jpg, size_t image_size)
{
    esp_jpeg_image_format_t format = JPEG_IMAGE_FORMAT_RGB888;
    esp_jpeg_image_scale_t scale = JPEG_IMAGE_SCALE_0;
    
    size_t width = 0;
    size_t height = 0;
    uint8_t *pixels = NULL;

    bool status = image_decode_jpg_scale(image_jpg, image_size, 
                            &width, &height, &pixels, format, scale);
    if(status == false && pixels) {
        free(pixels);
        return -1;
    }
    ESP_LOGI(TAG, "height: %d width: %d", height, width); // DEBUG!!!
    
    int id = -1;
    if (JPEG_IMAGE_FORMAT_RGB888 == format) {
        id = face_recognition_recognize(recognizer, pixels, height, width, 3);
    } else if (JPEG_IMAGE_FORMAT_RGB565 == format){
        // ...
        // TODO!!!
    }
    free(pixels);
    return id; 
}

// Recognize face from jpg file
int face_recognition_recognize(FR &recognizer, const char *path)
{
    struct stat st = {};
    if (stat(path, &st) != 0)   { // file not found
        perror(path);
        return -1;
    }
    
    size_t image_size = st.st_size; // file size
    if (image_size == 0)
        return -1;
            
    uint8_t *image_jpg = (uint8_t *)malloc(image_size);
    if (image_jpg == NULL)
        return -1;
    
    size_t bytes = 0;
    FILE *fp = fopen(path, "rb");
    if (fp) {
        bytes = fread(image_jpg, 1, image_size, fp);
        fclose(fp);        
        if (bytes != image_size) {
            free(image_jpg);
            return -1;
        }
    }
    
    int id = face_recognition_recognize(recognizer, image_jpg, image_size);
        
    free(image_jpg);
    return id; 
}
