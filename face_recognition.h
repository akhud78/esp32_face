#pragma once
#include <stdint.h>
#include <string>

#include "face_recognition_112_v1_s8.hpp"
typedef FaceRecognition112V1S8 FR;      // S8  model v1 size: 112 x 112 x 3

void face_recognition_enrolled_print(FR &recognizer);
std::string face_recognition_name(FR &recognizer, int id);

// Enroll faces
int face_recognition_enroll(FR &recognizer, uint8_t *image_jpg, size_t image_size, const char *name);
int face_recognition_enroll(FR &recognizer, const char *path, const char *name);
int face_recognition_enroll(FR &recognizer, const char *path_dir);

// Recognize face
int face_recognition_recognize(FR &recognizer, const uint8_t *image_bgr, int height, int width, int channel); 
int face_recognition_recognize(FR &recognizer, uint8_t *image_jpg, size_t image_size);
int face_recognition_recognize(FR &recognizer, const char *path);
