#include <esp_log.h>
#include <esp_system.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "unity.h"
#include "sdkconfig.h"

#include "image.h"
#include "sdcard.h"
#include "storage.h"
#include "face_detection.h"

#define FD_DIR      SDCARD_BASE_PATH "/" CONFIG_FACE_TEST_DIR 

static const char *TAG = "test_face_detection";

static const char *s_jpg_files[] = {
    FD_DIR "/roxanne_1.jpg", // height: 218 width: 178
    FD_DIR "/roxanne_2.jpg", // height: 275 width: 183
    FD_DIR "/roxanne_3.jpg", // height: 275 width: 183
    FD_DIR "/megan_1.jpg",   // height: 282 width: 178
    FD_DIR "/megan_2.jpg",   // height: 225 width: 225
    FD_DIR "/megan_3.jpg",   // height: 201 width: 251
    FD_DIR "/soto_1.jpg",    // height: 218 width: 178
    FD_DIR "/soto_2.jpg",    // height: 280 width: 218
    FD_DIR "/soto_3.jpg",    // height: 175 width: 287
    FD_DIR "/miller_1.jpg",  // height: 218 width: 178
    FD_DIR "/miller_2.jpg",  // height: 183 width: 275
    FD_DIR "/miller_3.jpg",  // height: 183 width: 275
};

static const char s_jpg_sandra[] = FD_DIR "/IM316_1.JPG";  // Sandra 320x240

static const size_t WIDTH = 400;
static const size_t HEIGHT = 320;

TEST_CASE("face detection", "[face]")
{        
    TEST_ASSERT_EQUAL(ESP_OK, sdcard_mount());
    esp_jpeg_image_scale_t scale = JPEG_IMAGE_SCALE_0;
    esp_jpeg_image_format_t format = JPEG_IMAGE_FORMAT_RGB888;
    
    int imax = sizeof(s_jpg_files)/sizeof(char*);
    bool detected = false;
    
    for (int i=0; i<imax; i++) {
        size_t width = 0;
        size_t height = 0;
        uint8_t *out = NULL;
        
        char *path = (char*)s_jpg_files[i];
        
        ESP_LOGI(TAG, "file: %s", path); // DEBUG!!!
        bool status = image_load_jpg_scale(path, &width, &height, &out, format, scale);
        if(status == false) {
            if (out) { 
                heap_caps_free(out);
                //free(out);
            }
            continue;
        }
        ESP_LOGI(TAG, "height: %d width: %d", height, width); // DEBUG!!!

        detected = false;
        // TWO_STAGE    
        if (face_detection((uint8_t *)out, height, width, 3, true)) {
            detected = true;
        }
        //free(out);        
        heap_caps_free(out);
    }
    TEST_ASSERT_TRUE(detected);    
    sdcard_unmount();
}








