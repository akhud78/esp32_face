#include <iostream>
#include <esp_log.h>
#include <esp_system.h>
#include <string.h>

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "unity.h"
#include "sdkconfig.h"

#include "image.h"
#include "sdcard.h"
#include "ffs.h"
#include "storage.h"
#include "face_recognition.h"

#define FR_TEST_DIR     SDCARD_BASE_PATH "/" CONFIG_FACE_TEST_DIR   
#define FR_SETUP_DIR    SDCARD_BASE_PATH "/" CONFIG_FACE_SETUP_DIR 

// --- Enrolled faces ---
/*
    roxanne_1.jpg   height: 218 width: 178
    megan_1.jpg     height: 282 width: 178
    soto_1.jpg      height: 218 width: 178
    miller_1.jpg    height: 218 width: 178

*/

// --- Test faces ---
static const char s_test_jpg_roxanne[] =    FR_TEST_DIR "/roxanne_2.jpg"; 
static const char s_test_jpg_megan[] =      FR_TEST_DIR "/megan_2.jpg";  
static const char s_test_jpg_soto[] =       FR_TEST_DIR "/soto_2.jpg";
static const char s_test_jpg_miller[] =     FR_TEST_DIR "/miller_2.jpg";
static const char s_test_jpg_unknown[] =     FR_TEST_DIR "/IM316_1.JPG";

TEST_CASE("face recognition", "[face]")
{    
    TEST_ASSERT_EQUAL(ESP_OK, sdcard_mount());
    
    printf("esp_get_free_heap_size: %d\n", esp_get_free_heap_size());
    
    FR recognizer;
    TEST_ASSERT_EQUAL(0, face_recognition_enroll(recognizer, FR_SETUP_DIR));
    face_recognition_enrolled_print(recognizer);
    
    // --- Test Roxanne ---         
    int id = face_recognition_recognize(recognizer, s_test_jpg_roxanne);
    TEST_ASSERT_TRUE(id > 0);
    std::string  name = face_recognition_name(recognizer, id);
    name.erase(name.find_first_of('_'));
    TEST_ASSERT_EQUAL_STRING( "roxanne", name.c_str());
    
    // --- Test Megan --- 
    id = face_recognition_recognize(recognizer, s_test_jpg_megan);
    TEST_ASSERT_TRUE(id > 0);
    name = face_recognition_name(recognizer, id);
    name.erase(name.find_first_of('_'));
    TEST_ASSERT_EQUAL_STRING( "megan", name.c_str());
    
    // --- Test Soto --- 
    id = face_recognition_recognize(recognizer, s_test_jpg_soto);
    TEST_ASSERT_TRUE(id > 0);
    name = face_recognition_name(recognizer, id);
    name.erase(name.find_first_of('_'));
    TEST_ASSERT_EQUAL_STRING( "soto", name.c_str());
    
    // --- Test Miller --- 
    id = face_recognition_recognize(recognizer, s_test_jpg_miller);
    TEST_ASSERT_TRUE(id > 0);
    name = face_recognition_name(recognizer, id);
    name.erase(name.find_first_of('_'));
    TEST_ASSERT_EQUAL_STRING( "miller", name.c_str());
        
    // --- Test Unknown --- 
    id = face_recognition_recognize(recognizer, s_test_jpg_unknown);
    TEST_ASSERT_TRUE(id < 0);
    
    sdcard_unmount();
    
}




