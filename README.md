# esp32_face
Face detection and recognition component

## Requirements
- [ESP-IDF v5.0.2](https://docs.espressif.com/projects/esp-idf/en/v5.0.2/esp32s3/index.html)

## Setup
- Add [image](https://gitlab.com/akhud78/esp32_image) component. See `image` [setup](https://gitlab.com/akhud78/esp32_image#setup).
```
$ cd ~/esp/my_project/components
$ git submodule add https://gitlab.com/akhud78/esp32_image image
```
- Add [esp-dl](https://github.com/espressif/esp-dl) component
```
$ git submodule add -b idfv5.0 https://github.com/espressif/esp-dl
```
- Add [face](https://github.com/akhud78/esp32_face) component
```
$ git submodule add https://gitlab.com/akhud78/esp32_face face
```

## Configuration
- [Kconfig](Kconfig)
```
(Top) -> Component config -> Face Configuration

(face_test) Test image directory
(face_setup) Setup image directory
[*] Print detection results
[*] Draw detection results
```
- Copy [face_test](face_test) and [face_setup](face_setup) directories with JPG to SD card

## Test
```
### Starting interactive test menu ####
...

Press ENTER to see the list of tests.

Here's the test menu, pick your combo:
(1)	"face detection" [face]
(2)	"face recognition" [face]
...
```


