#pragma once
#include <stdint.h>

// Face Detection
bool face_detection(const uint8_t *image, int height, int width, int channel, bool two_stage);
bool face_detection(const uint16_t *image, int height, int width, int channel, bool two_stage);


