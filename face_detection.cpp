// https://github.com/espressif/esp-dl/blob/master/examples/human_face_detect/main/app_main.cpp
#include <stdexcept>
#include <iostream>

#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <esp_log.h>
#include "sdkconfig.h"

#include "dl_tool.hpp"
#include "who_ai_utils.hpp"
#include "face_detection.h"

//static const char *TAG = "face";

#include "human_face_detect_msr01.hpp"
#include "human_face_detect_mnp01.hpp"

bool face_detection(const uint8_t *image, int height, int width, int channel, bool two_stage)
{    
    bool detected = false;
    dl::tool::Latency latency; 
    
    // initialize
    if (two_stage) { // detect by two-stage which is more accurate but slower(with keypoints)

        HumanFaceDetectMSR01 s1(0.1F, 0.5F, 10, 0.2F);
        HumanFaceDetectMNP01 s2(0.5F, 0.3F, 5);
        
        latency.start();
        std::list<dl::detect::result_t> &candidates = s1.infer((uint8_t *)image, {height, width, channel});
        std::list<dl::detect::result_t> &results = s2.infer((uint8_t *)image, {height, width, channel}, candidates);
        latency.end();
        // display    
    #ifdef CONFIG_FACE_DETECTION_PRINT
        //latency.print("Inference latency two-stage");
        print_detection_result(results);
    #endif // CONFIG_FACE_DETECTION_PRINT
        
    #ifdef CONFIG_FACE_DETECTION_DRAW
        draw_detection_result((uint8_t *)image, height, width, results);    
    #endif // CONFIG_FACE_DETECTION_DRAW
        
        int i = 0;
        for (std::list<dl::detect::result_t>::iterator prediction = results.begin(); 
                                    prediction != results.end(); prediction++, i++) {
                                        
            if (prediction->score > 0.2)
                detected = true;
        }

    } else { // detect by one-stage which is less accurate but faster(without keypoints)
        HumanFaceDetectMSR01 s1(0.3F, 0.5F, 10, 0.2F);
        
        latency.start();
        std::list<dl::detect::result_t> &results = s1.infer((uint8_t *)image, {height, width, channel});
        latency.end();
        // display    
    #ifdef CONFIG_FACE_DETECTION_PRINT
        //latency.print("Inference latency one-stage");
        print_detection_result(results);
    #endif // CONFIG_FACE_DETECTION_PRINT
        
    #ifdef CONFIG_FACE_DETECTION_DRAW
        draw_detection_result((uint8_t *)image, height, width, results);    
    #endif // CONFIG_FACE_DETECTION_DRAW
        
        int i = 0;
        for (std::list<dl::detect::result_t>::iterator prediction = results.begin(); 
                                    prediction != results.end(); prediction++, i++) {
                                        
            if (prediction->score > 0.2)
                detected = true;
        }
    }
    
    return detected;
}

bool face_detection(const uint16_t *image, int height, int width, int channel, bool two_stage)
{    
    bool detected = false;
    dl::tool::Latency latency; 
    
    // initialize
    if (two_stage) { // detect by two-stage which is more accurate but slower(with keypoints)

        HumanFaceDetectMSR01 s1(0.1F, 0.5F, 10, 0.2F);
        HumanFaceDetectMNP01 s2(0.5F, 0.3F, 5);
        
        latency.start();
        std::list<dl::detect::result_t> &candidates = s1.infer((uint16_t *)image, {height, width, channel});
        std::list<dl::detect::result_t> &results = s2.infer((uint16_t *)image, {height, width, channel}, candidates);
        latency.end();
        // display    
    #ifdef CONFIG_FACE_DETECTION_PRINT
        //latency.print("Inference latency two-stage");
        print_detection_result(results);
    #endif // CONFIG_FACE_DETECTION_PRINT
        
    #ifdef CONFIG_FACE_DETECTION_DRAW
        draw_detection_result((uint16_t *)image, height, width, results);    
    #endif // CONFIG_FACE_DETECTION_DRAW
        
        int i = 0;
        for (std::list<dl::detect::result_t>::iterator prediction = results.begin(); 
                                    prediction != results.end(); prediction++, i++) {
                                        
            if (prediction->score > 0.2)
                detected = true;
        }

    } else { // detect by one-stage which is less accurate but faster(without keypoints)
        HumanFaceDetectMSR01 s1(0.3F, 0.5F, 10, 0.2F);
        
        latency.start();
        std::list<dl::detect::result_t> &results = s1.infer((uint16_t *)image, {height, width, channel});
        latency.end();
        // display    
    #ifdef CONFIG_FACE_DETECTION_PRINT
        //latency.print("Inference latency one-stage");
        print_detection_result(results);
    #endif // CONFIG_FACE_DETECTION_PRINT
        
    #ifdef CONFIG_FACE_DETECTION_DRAW
        draw_detection_result((uint16_t *)image, height, width, results);    
    #endif // CONFIG_FACE_DETECTION_DRAW
        
        int i = 0;
        for (std::list<dl::detect::result_t>::iterator prediction = results.begin(); 
                                    prediction != results.end(); prediction++, i++) {
                                        
            if (prediction->score > 0.2)
                detected = true;
        }
    }
    
    return detected;
}
